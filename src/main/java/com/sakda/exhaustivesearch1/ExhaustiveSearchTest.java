/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sakda.exhaustivesearch1;

/**
 *
 * @author PC Sakda
 */
public class ExhaustiveSearchTest {
    public static void main(String[] args) {
        int[] a = {2, 6, 3, 8, 9, 4, 8};
        showInput(a);
        EXS exs = new EXS(a);
        exs.process();
        exs.sum(); //sum and show result

    }

    private static void showInput(int[] a) {
        System.out.print("Input is: ");
        for (int i = 0; i < a.length; i++) {
            System.out.print(a[i] + " ");
        }
        System.out.println("");
    }
    }
